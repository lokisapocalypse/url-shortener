<?php
    $conn = new mysqli('localhost', 'url_user', 'password', 'url');

    $code = $_GET['code'];
    $query = sprintf(
        'SELECT url FROM url_list WHERE shortened = "%s"',
        $conn->real_escape_string($code)
    );

    $result = $conn->query($query);

    $url = '/';
    if ($result && $result->num_rows == 1) {
        $url = $result->fetch_assoc()['url'];
    }

    header('Location: '.$url);
?>
