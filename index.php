<html>
    <head>
        <style type="text/css">
            #url { display: none; }
        </style>
    </head>
    <body>
        <h1>URL Shortener</h1>
        <p>Enter the url to shorten:</p>
        <input id="url-field" name="url" type="text">
        <button type="button" id="submit">Shorten</button>
        <a href="#" id="url" target="_blank"></a>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript">
            $('#submit').on('click', function (e) {
                $.get('/url.php', { url: $('#url-field').val() }, function (response) {
                    if (response != '') {
                        $('#url').attr('href', response);
                        $('#url').html(response);
                        $('#url').show();
                    }
                });
            });
        </script>
    </body>
</html>
