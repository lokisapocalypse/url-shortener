<?php
    $conn = new mysqli('localhost', 'url_user', 'password', 'url');

    $url = $_GET['url'];
    $query = sprintf(
        'SELECT shortened FROM url_list WHERE url = "%s"',
        $conn->real_escape_string($url)
    );

    $result = $conn->query($query);

    if (!$result || $result->num_rows == 0) {
        $code = generateRandomCode();

        do {
            $urlQuery = sprintf(
                'SELECT id FROM url_list WHERE shortened = "%s%"',
                $code
            );
            $urlResult = $conn->query($urlQuery);
        } while ($urlResult);

        $query = sprintf(
            'INSERT INTO url_list (shortened, url) VALUES ("%s", "%s")',
            $conn->real_escape_string($code),
            $conn->real_escape_string($url)
        );
        $conn->query($query);
    } else {
        $code = $result->fetch_assoc()['shortened'];
    }

    echo 'http://url.fusaniapplications.com/r/'.$code;
?>

<?php
function generateRandomCode()
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < 6; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
